﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OwlBird : Bird
{
    [SerializeField]
    public float _explodeForce;
    public bool _hasExplode = false;

    public void Explode()
    {
    	if (!_hasExplode) {
    		Collider.radius = Collider.radius * _explodeForce;
    		_hasExplode = true;
    	}

    	StartCoroutine(Destroying(0.01f));
    }

    void OnCollisionEnter2D(Collision2D col)
    {
    	Explode();
    }

    private IEnumerator Destroying(float second)
    {
    	yield return new WaitForSeconds(second);
    	Destroy(gameObject);
    }
}
