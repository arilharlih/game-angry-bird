﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

	public SlingShooter SlingShooter;
	public TrailController TrailController;
	public List<Bird> Birds;
	public List<Enemy> Enemies;
	private Bird _shotBird;
	public BoxCollider2D TapCollider;
	public string sceneLevel;

	

    // Start is called before the first frame update
    void Start()
    {
    	for (int i = 0; i < Birds.Count; i++) {
    		Birds[i].OnBirdDestroyed += ChangeBird;
    		Birds[i].OnBirdShot += AssignTrail;
    	}

    	for (int i = 0; i < Enemies.Count; i++) {
    		Enemies[i].OnEnemyDestroyed += CheckGameEnd;
    	}

    	TapCollider.enabled = false;
        SlingShooter.InitiateBird(Birds[0]);
        _shotBird = Birds[0];
    }

    public void ChangeBird()
    {
    	TapCollider.enabled = false;

    	Birds.RemoveAt(0);

    	if (Birds.Count > 0) {
    		SlingShooter.InitiateBird(Birds[0]);
    		_shotBird = Birds[0];
    	}
    }

    public void CheckGameEnd(GameObject destroyedEnemy)
    {
    	for (int i = 0; i < Enemies.Count; i++) {
    		if (Enemies[i].gameObject == destroyedEnemy) {
    			Enemies.RemoveAt(i);
    			break;
    		}
    	}

    	if (Enemies.Count == 0) {
    		StartCoroutine(MoveScene(2));
    	}
    }

    public void AssignTrail(Bird bird)
    {
    	TrailController.SetBird(bird);
    	StartCoroutine(TrailController.SpawnTrail());
    	TapCollider.enabled = true;
    }

    void OnMouseUp()
    {
    	if (_shotBird != null) {
    		_shotBird.OnTap();
    	}
    }

    private IEnumerator MoveScene(float second)
    {
    	yield return new WaitForSeconds(second);

    	SceneManager.LoadScene(sceneLevel);
    }
}
